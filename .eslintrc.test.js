module.exports = {
	parserOptions: {
		project: './tsconfig.json',
		tsconfigRootDir: __dirname,
	},
	extends: ['web3-base/ts-jest'],
};
